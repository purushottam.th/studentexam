<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentExamPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_exam_pivot', function (Blueprint $table) {
            $table->increments('sep_id');
            $table->integer('student_id')->unsigned();
            $table->integer('exam_id')->unsigned();
            $table->string('mark_obtain', 100)->default(0);
            $table->string('out_of_marks', 50)->default(0);
            $table->string('appeared', 15)->default('not appeared');
            $table->foreign('exam_id')->references('exam_id')->on('exam');
            $table->foreign('student_id')->references('student_id')->on('students');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_exam_pivot');
    }
}
