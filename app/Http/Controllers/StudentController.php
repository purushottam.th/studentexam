<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\User;
use App\FileUpload;
use App\exam;
use App\question;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{

    protected $studentModel;
    protected $userModel;
    protected $examModel;
    protected $questionModel;

 public function __construct() {
        $this->setStudentModel();
        $this->setUserModel();
        $this->setExamModel();
        $this->setQuestionModel();
    }

    public function setStudentModel() {
        $local = new Student();
        $this->studentModel = $local;
    }

    public function getStudentModel() {
        return $this->studentModel;
    }
    public function setUserModel() {
        $local = new User();
        $this->userModel = $local;
    }

    public function getUserModel() {
        return $this->userModel;
    }

    public function setExamModel() {
        $local = new exam();
        $this->examModel = $local;
    }

    public function getExamModel() {
        return $this->examModel;
    }
    public function setQuestionModel() {
        $local = new question();
        $this->questionModel = $local;
    }

    public function getQuestionModel() {
        return $this->questionModel;
    }

    /**
     * Get the view for student registration
     *
     * @return Response
     */

    public function getStudentRegister()
    {
        $student_id = $this->getStudentModel()->max('student_id');

        return view('student.studentRegister', compact('student_id'));
    }

    /**
     * Save student in the database.
     *
     * @param  Request  $request
     * @return Response
     */

    public function postStudentRegister(Request $request)
    {


        $this->validate($request, [
            'first_name' => 'required|min:3|max:255',
            'last_name' => 'required|min:3|max:255',
            'course_name' => 'required|min:3|max:255',
            'class' => 'required|min:3|max:255',
            'sex' => 'required|integer',
            'dateOfBirth' => 'required|date_format:Y-m-d',
            'email' => 'required|email|max:50|unique:students',
            'username' => 'required|min:5|max:12|unique:users',
            'phone' => 'required|max:10',
            'photo' => 'required|mimes:jpeg,bmp,png,jpg,x-png',
          ]
        );

        $student = $this->getStudentModel();
        $student->first_name = $request->first_name;
        $student->last_name  = $request->last_name;
        $student->sex = $request->sex;
        $student->dob = $request->dateOfBirth;
        $student->email = $request->email;
        $student->course_name = $request->course_name;
        $student->class = $request->class;
        $student->phone = $request->phone;
        $student->village = $request->village;
        $student->district = $request->district;
        $student->taluka = $request->taluka;
        $student->current_address = $request->current_address;
        $student->dateregistered = $request->dateregistered;
        $student->user_id = $request->user_id;
        $student->photo = FileUpload::photo($request, 'photo');
        $student->created_at = date("Y-m-d H:i:s");
        $student->updated_at = date("Y-m-d H:i:s");
        $login = $this->getUserModel();

        $login->role_id = '2';
        $login->active = '1';
        $login->name = $request->first_name;
        $login->username = $request->username;
        $login->email = $request->email;
        $login->password = bcrypt($request->phone);
        $login->remember_token = str_random(10);
        if($student->save())
        {
          $login->save();
            $student_id = $student->student_id;

            // Save student id in the given class

            session()->flash('message', 'Student has been successfully registered!');

            return redirect()->route('studentInfo', ['student_id' => $student_id]);
        }
    }

    /**
     * Get informations of students.
     *
     * @param  Request  $request
     * @return Response
     */

    public function studentInfo(Request $request)
    {

            $students = Student::select(DB::raw('student_id,
                                                  first_name,
                                                  last_name,
                                                  CONCAT(first_name, " ", last_name) as full_name,
                                                  (CASE WHEN sex=0 THEN "M" ELSE "F" END) as sex,
                                                  dob'))
                          ->paginate(10);

        return view('student.studentList', compact('students'));
    }
    public function studentExamDashboard(Request $request){
      $user_id=Auth::id();
      $useremail = $this->getUserModel()->select('email')->where('id',$user_id)->value('email');
      $student_id = $this->getStudentModel()->select('student_id')->where('email',$useremail)->value('student_id');
      $filter=array("student_id" => $student_id,"exam_id" => '');      
      $examAssignArray = $this->getExamModel()->getAssignExamData($filter);      
      
      return view('student.studentAssignExam', compact('examAssignArray'));

    }

    public function startExam(Request $request){
      $examDetails = $this->getExamModel()->getExamData($request->exam_Id);
      $questionDetails = $this->getQuestionModel()->getQuestionList($request->exam_Id);
      $studentId= $request->student_id;
      $examId= $request->exam_Id;
      return view('student.studentStartExam', compact('examDetails','questionDetails','studentId','examId'));      
    }

    public function postExamSubmit(Request $request){
      $marks_obtain = 0;
          $questionDetails = $this->getQuestionModel()->getQuestionList($request->examId);
          $examDetails = $this->getExamModel()->getExamData($request->examId);
          foreach ($questionDetails as $question_value) {
            if($request->{"exam_".$question_value['question_id']} == $question_value['question_answer']){
              $marks_obtain=$marks_obtain+$examDetails['per_question_marks'];
            }
          }
          $final_res=array('mark_obtain'=>$marks_obtain,'out_of_marks'=>$examDetails['total_marks'],'appeared'=>'Appeared','updated_at'=>date("Y-m-d H:i:s"));
           $result = $this->getExamModel()->updateExam($final_res,$request->studentId,$request->examId);
           if($result){
            session()->flash('message', 'Exam succsessfully submitted!');
            return redirect()->route('studentExamDashboard');
           }
    }
}
