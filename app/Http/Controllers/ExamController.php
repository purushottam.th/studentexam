<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\exam;
use App\question;
use App\Student;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class ExamController extends Controller
{

    protected $examModel;
    protected $questionModel;
    protected $studentModel;

    public function __construct() {
        $this->setExamModel();
        $this->setQuestionModel();
        $this->setStudentModel();
    }

    public function setExamModel() {
        $local = new exam();
        $this->examModel = $local;
    }

    public function getExamModel() {
        return $this->examModel;
    }
    public function setQuestionModel() {
        $local = new question();
        $this->questionModel = $local;
    }

    public function getQuestionModel() {
        return $this->questionModel;
    }


    public function setStudentModel() {
        $local = new Student();
        $this->studentModel = $local;
    }

    public function getStudentModel() {
        return $this->studentModel;
    }
    
    /**
     * Get the view for Exam Create
     *
     * @return Response
     */

    public function getExamForm()
    {
        return view('Exam.examCrate');
    }

    /**
     * Save Exam in the database.
     *
     * @param  Request  $request
     * @return Response
     */

    public function postExamCreate(Request $request)
    {
        $this->validate($request, [
            'exam_title' => 'required|min:3|max:255',
            'number_question' => 'required|integer|min:1',
            'exam_time' => 'required|date_format:H:i:s',
            'marks_per_ques' => 'required|integer|min:1',
            'examDate' => 'required|date_format:Y-m-d'
            ]
        );

        $exam = $this->getExamModel();
        $exam->exam_title = $request->exam_title;
        $exam->exam_date  = $request->examDate;
        $exam->duration = $request->exam_time;
        $exam->no_question = $request->number_question;
        $exam->per_question_marks = $request->marks_per_ques;
        $exam->total_marks = ($request->number_question * $request->marks_per_ques);
        $exam->user_id = $request->user_id;
        $exam->created_at = date("Y-m-d H:i:s");
        $exam->updated_at = date("Y-m-d H:i:s");
        if($exam->save())
        {
            session()->flash('message', 'Exam has been successfully Created!');

            return redirect()->route('examInfo');
        }
    }

    /**
     * Get informations of Exam.
     *
     * @param  Request  $request
     * @return Response
     */

    public function examInfo(Request $request)
    {
        $exam = $this->getExamModel()->getExamGrid();
        return view('Exam.examList', compact('exam'));
    }

    /**
     * Get Question list.
     *
     * @param  Request  $request
     * @return Response
     */
    public function questionInfo(Request $request){
      $examDetails = $this->getExamModel()->getExamData($request->examId);
      $questionDetails = $this->getQuestionModel()->getQuestionList($request->examId);      
      return view('Exam.questionList', compact('examDetails','questionDetails'));
    }

    /**
     Get of Exam Question Form.
     *
     * @param  Request  $request
     * @return Response
     */
    public function questionAddForm(Request $request){
      $question_id=0;
      $examDetails = $this->getExamModel()->getExamData($request->examId);
      $questionCount = $this->getQuestionModel()->getQuestionCount($request->examId);
      $questionRemain = $examDetails['no_question'] - $questionCount;
      $question_id  += $this->getQuestionModel()->max('question_id');
      return view('Exam.questionCreate', compact('examDetails','questionCount','questionRemain','question_id'));
    }

    /**
     * Save Exam question form.
     *
     * @param  Request  $request
     * @return Response
     */
    public function questionPostForm(Request $request){

      $question = $this->getQuestionModel();
      for($i=1 ; $i<= $request->noOfQuestion ;$i++){
        $id=$i + $request->firstQuesId;
        $question_title=$request->{"question_title_".$id};
        $question_answer=$request->{"question_answer_".$id};
        $question_option=$request->{"remain_opt_ques_".$id};
        $exam_id = $request->examId;
        $created_at = date("Y-m-d H:i:s");
        $updated_at = date("Y-m-d H:i:s"); 
        if(!empty($question_title) && !empty($question_answer) && !empty($question_option)){
          $ques_data[] = array('question_title'=>$question_title, 'question_answer'=> $question_answer,'question_option'=> $question_option,'exam_id'=> $exam_id,'created_at'=> $created_at,'updated_at'=> $updated_at);
        }
      }
      $question->insert($ques_data);
      $examDetails = $this->getExamModel()->getExamData($request->examId);
      $questionDetails = $this->getQuestionModel()->getQuestionList($request->examId);      
      return view('Exam.questionList', compact('examDetails','questionDetails'));
  }


    /**
     Get of Exam Question Form.
     *
     * @param  Request  $request
     * @return Response
     */
    public function assignExam(Request $request){
      $assign_exam_student_id = $this->getExamModel()->getAssignExamStudent($request->examId);
      $student_array = $this->getStudentModel()
                            ->select(DB::raw('student_id,
                            CONCAT(first_name, " ", last_name) as full_name'))->whereNotIn('student_id',$assign_exam_student_id)->pluck('full_name','student_id');
      $examId=$request->examId;
      return view('Exam.assignStudent', compact('student_array','examId'));
    }

    /**
     Save of Exam Question Form.
     *
     * @param  Request  $request
     * @return Response
     */
    public function postAssign(Request $request){
 
      foreach ($request->assign_student as  $value) {
        $assignStudent[]= array('student_id' =>$value,'exam_id' =>$request->examId,'created_at' =>date("Y-m-d H:i:s"),'updated_at' =>date("Y-m-d H:i:s"));
      }
      $assignExams=$this->getExamModel()->assignStudents($assignStudent);
      if($assignExams)
        {
            session()->flash('message', 'Student has been Assign to Exam!');
            return redirect()->route('examInfo');
        }
    }


    /**
     * Get Assign student exam.
     *
     * @param  Request  $request
     * @return Response
     */
    public function assignExamFetch(Request $request){
      $filter=array("student_id" => '',"exam_id" => '');
      if(isset($request->studentId)){
        $filter['student_id']=$request->studentId;
      }
      if(isset($request->examId)){
        $filter['exam_id']=$request->examId;

      }
      $examAssignArray = $this->getExamModel()->getAssignExamData($filter);      
      return view('Exam.assignExamList', compact('examAssignArray'));
    }

}
