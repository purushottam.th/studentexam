<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $roles = $this->getRequiredRoleForRoute($request->route());

        $role=$request->user();
        
        if($request->user()->hasRole($roles) || !$roles)
        {
            Session()->put('role', 'Admin');
            return $next($request);
        }elseif($role['role_id'] == '2'){
            Session()->put('role', 'student');
            return redirect()->route('studentExamDashboard');
        }

        return redirect('/noPermission');
    }

    private function getRequiredRoleForRoute($route)
    {
        $actions = $route->getAction();
        return isset($actions['roles']) ? $actions['roles'] : null;
    }
}
