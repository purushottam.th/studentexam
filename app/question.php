<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class question extends Model
{
    protected $table = 'question';
    protected $primaryKey = 'question_id';
    public $timestamps = false;

    public function getQuestionList($examId){
    	return $data=question::select('*')->where('exam_id',$examId)->get()->toArray();
    }

    public function getQuestionCount($examId){
    	return $data=question::select('*')->where('exam_id',$examId)->count();
    }
}
