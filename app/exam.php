<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class exam extends Model
{
    protected $table = 'exam';
    protected $primaryKey = 'exam_id';
    public $timestamps = false;

    public function getExamGrid(){
    	return $data=exam::select('*')->orderBy('exam_date','DESC')
            ->paginate(10);
    }
    public function getExamData($examId){
    	return $data=exam::select('*')->where('exam_id',$examId)->get()->first();
    }
    public function getAssignExamStudent($examId){
    	return $data = DB::table('student_exam_pivot')->select('student_id')->where('exam_id',$examId)->pluck('student_id');
    }
    public function assignStudents($assignStudent){
    	return $data = DB::table('student_exam_pivot')->insert($assignStudent); 
    }
    public function getAssignExamData($filter){
    	$data = DB::table('student_exam_pivot')
    					->leftjoin('students','students.student_id','=','student_exam_pivot.student_id')
    					->leftjoin('exam','exam.exam_id','=','student_exam_pivot.exam_id')
    					->select('mark_obtain','out_of_marks','appeared','exam_title','exam_date','duration','total_marks','no_question','per_question_marks','first_name','last_name','student_exam_pivot.exam_id','student_exam_pivot.student_id');
    	if(!empty($filter['student_id'])){
    		$data->where('student_exam_pivot.student_id',$filter['student_id']);
    	}
    	if(!empty($filter['exam_id'])){
    		$data->where('student_exam_pivot.exam_id',$filter['exam_id']);
    	}
    	return $data->orderBy('exam_date','DESC')->get()->toArray();
    }
    public function updateExam($update_data,$studentId,$examId){
    	return $data = DB::table('student_exam_pivot')
    						->where('student_id', $studentId)
    						->where('exam_id', $examId)
        					->update($update_data); 
    }

}
