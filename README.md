<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

### Premium Partners

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[Many](https://www.many.co.uk)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- **[OP.GG](https://op.gg)**

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).


## ======= Tech Spec =============

	[*] php 7.x. && php7.0/fpm/pool.d/www.conf has to be listen = 127.0.0.1:9000

	[*] Install Composer globally ( reference link https://getcomposer.org/download/)
	
	[*] Laravel 7.24
	
## ======= PRE - REQUISITE :: On Ubuntu 16.04/14.04 installing PHP =========


	Step 1 : sudo apt-get update && apt-get purge php5.6-common && apt-get --purge autoremove
	````		 
	Step 2 : sudo apt-get install -y php7.0 php7.0-cgi php7.0-cli php7.0-fpm php7.0-common php7.0-curl php7.0-enchant php7.0-gd php7.0-imap php7.0-intl php7.0-ldap php7.0-mcrypt php7.0-readline php7.0-pspell php7.0-tidy php7.0-xmlrpc php7.0-xsl php7.0-json php7.0-sqlite3 php7.0-mysql php7.0-opcache php7.0-bz2 libapache2-mod-php7.0 php7.0-mbstring snmp-mibs-downloader php7.0-bcmath php-mongodb php7.0-dev
	sudo apt-get install libcurl4-openssl-dev pkg-config libssl-dev libsslcommon2-dev libsasl2-dev
	

	Step 3 : sudo apt-get update
	````	 



##  ========= ACTUAL STEPS FOR ALL LINUX DISTRO:: Installation from Student Git Repo =============


	Step 1  : sudo git clone https://gitlab.com/purushottam.th/studentexam.git .
	```` 

	Step 2  : 	Below Folder Creations
				
				sudo mkdir storage && sudo mkdir storage/public


	Step 3  : sudo chown -R {{ Your Logged In User }}:{{ Your Logged In User }} .
	````

	Step 4  : sudo chmod -R 777 .
	````

	Step 5  : sudo cp .env.example .env (Edit the database credentials)
	````

	Step 6  : composer update
	````


	Step 7  : sudo php artisan key:generate
	````


	Step 8  : composer dump-autoload
	````
	
	Step 9  : sudo php artisan migrate
	````
	
	Step 10  : php artisan cache:clear
	````
	
	Step 11  : php artisan view:clear	
	````
	
	Step 12  : php artisan route:clear
	````

	Step 13 : sudo chown -R nginx:nginx . {{ Your Web Server User }}:{{ Your Web Server User }}
	````

	Step 14 : sudo chmod -R 777 .
	````

	Step 15 : php artisan db:seed --class=RolesTablesSeeder
	````

	Step 16 : php artisan db:seed --class=UsersTablesSeeder
	````	


	



