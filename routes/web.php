<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => '/', 'uses' => 'LoginController@getLogin']);
Route::post('/login', ['as' => 'login', 'uses' => 'LoginController@postLogin']);

Route::get('/noPermission', function (){
    return view('permission.permission');
});

Route::group(['middleware' => ['authen']], function () {
    Route::get('/logout', ['as' => 'logout', 'uses' => 'LoginController@getLogout']);
    Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@dashboard']);
});

Route::group(['middleware' => ['authen', 'roles'], 'roles' => ['admin']], function () {
    // Student Controller
    Route::get('/student/register', ['as' => 'getStudentRegister', 'uses' => 'StudentController@getStudentRegister']);
    Route::post('/student/postRegister', ['as' => 'postStudentRegister', 'uses' => 'StudentController@postStudentRegister']);
    Route::get('/student/info', ['as' => 'studentInfo', 'uses' => 'StudentController@studentInfo']);
    
    // exam controller

    Route::get('/exam/create', ['as' => 'getExamCreate', 'uses' => 'ExamController@getExamForm']);
    Route::post('/exam/postCreate', ['as' => 'postExamCreate', 'uses' => 'ExamController@postExamCreate']);
    Route::get('/exam/info', ['as' => 'examInfo', 'uses' => 'ExamController@examInfo']);
    Route::get('/exam/question/info', ['as' => 'questionInfo', 'uses' => 'ExamController@questionInfo']);
    Route::get('/exam/question/addView', ['as' => 'questionAdd', 'uses' => 'ExamController@questionAddForm']);

    Route::post('/exam/question/postAdd', ['as' => 'postquestionCreate', 'uses' => 'ExamController@questionPostForm']);
    Route::get('/exam/assignExam', ['as' => 'assignExam', 'uses' => 'ExamController@assignExam']);
    Route::post('/exam/postAssign', ['as' => 'postExamAssign', 'uses' => 'ExamController@postAssign']);    
    Route::get('/exam/assignExamFetch', ['as' => 'assignExamFetch', 'uses' => 'ExamController@assignExamFetch']);

});


Route::group(['middleware' => 'authen'], function () {
    Route::get('/student/exam/dashboard', ['as' => 'studentExamDashboard', 'uses' => 'StudentController@studentExamDashboard']);

    Route::get('/student/exam/start', ['as' => 'startExam', 'uses' => 'StudentController@startExam']); 

    Route::post('/exam/submit', ['as' => 'postExamSubmit', 'uses' => 'StudentController@postExamSubmit']);   
});