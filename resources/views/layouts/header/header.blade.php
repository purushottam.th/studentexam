<header class="header dark-bg">
    <div class="toggle-nav">
        <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"></div>
    </div>

    <!--logo start-->
    <?php $role=session()->get('role'); ?>
    @if($role=='student')
    <a href="{{ route('studentExamDashboard') }}" class="logo">Student <span class="lite">Dashboard</span></a>
    @else
    <a href="{{ route('studentInfo') }}" class="logo">Super <span class="lite">Admin</span></a>
    @endif

    <!--logo end-->
    <div class="top-nav notification-row">
        <!-- notificatoin dropdown start-->
        <ul class="nav pull-right top-menu">

            <!-- user login dropdown start-->
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="profile-ava">
                                <img alt="" src="{{asset('img/avatar1_small.jpg')}}">
                            </span>
                    <span class="username"><?php print_r(session()->get('username')); ?></span>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu extended logout">
                    <div class="log-arrow-up"></div>
                    <li>
                        <a href="{{ route('logout') }}"><i class="icon_key_alt"></i> Log Out</a>
                    </li>
                </ul>
            </li>
            <!-- user login dropdown end -->
        </ul>
        <!-- notificatoin dropdown end-->
    </div>
</header>
<!--header end-->