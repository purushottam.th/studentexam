<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">
    <?php $role=session()->get('role'); ?>

            @if($role=='student')
            <li class="sub-menu">
                <a href="javascript:;" class="">
                    <i class="icon_table"></i>
                    <span>Exam</span>
                    <span class="menu-arrow arrow_carrot-right"></span>
                </a>
                <ul class="sub">
                    <li><a class="" href="{{ route('getExamCreate') }}">Exam</a></li>
                </ul>
            </li>
            @else
            <li class="sub-menu">
                <a href="javascript:;" class="">
                    <i class="icon_desktop"></i>
                    <span>Student</span>
                    <span class="menu-arrow arrow_carrot-right"></span>
                </a>
                <ul class="sub">
                    <li><a class="" href="{{ route('getStudentRegister') }}">Create Student</a></li>
                    <li><a class="" href="{{ route('studentInfo') }}">Student List</a></li>
                </ul>
            </li>

            <li class="sub-menu">
                <a href="javascript:;" class="">
                    <i class="icon_table"></i>
                    <span>Exam</span>
                    <span class="menu-arrow arrow_carrot-right"></span>
                </a>
                <ul class="sub">
                    <li><a class="" href="{{ route('getExamCreate') }}">Exam create</a></li>
                    <li><a class="" href="{{ route('examInfo') }}">Exam List</a></li>
                </ul>
            </li>
            @endif
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>