@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-file-text-o"></i>{{ $examDetails['exam_title'] }}</h3>
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="#">Home</a></li>
                <li><i class="icon_document_alt"></i>Exam</li>
                <li><i class="fa fa-file-text-o"></i>Question of {{ $examDetails['exam_title'] }}</li>
            </ol>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            @if($examDetails['no_question'] > count($questionDetails) )
            <a href="{{ route('questionAdd') }}?examId={{ $examDetails['exam_id'] }}"><button class="btn btn-default btn-save"> Add Question</button></a>
            @endif
        </div>
        <div class="panel-body">
            @if(count($questionDetails)==0)
            <span>no question added in this exam</span>
            @endif
            <?php $q=1; ?>
                @foreach($questionDetails as $questions)
            <div class="raw">
            <div class="col-lg-9 col-md-9 col-sm-9">
            <h3>Que {{ $q++ }}. {{ $questions['question_title'] }} ?</h3>
            <div class="row">
                <?php $options = $questions['question_option'].",".$questions['question_answer'];
                $optionArr= explode(",", $options);
                $first = shuffle($optionArr);                
                 ?>
                 @foreach($optionArr as $optionVal)
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <input type="radio" checked>
                    <label for="male">{{ $optionVal }}</label>
                </div>
                @endforeach
            </div>
            <h4>Answer. {{ $questions['question_answer'] }}</h4>
            </div>
            </div>
            @endforeach
            
        </div>
        <div class="footer">
        </div>
    </div>

@endsection

@section('script')
@endsection