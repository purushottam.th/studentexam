@extends('layouts.master')

@section('content')
    <style type="text/css">
        fieldset {
            margin-top: 5px;
        }

        fieldset legend {
            display: block;
            width: 100%;
            padding: 0;
            font-size: 15px;
            line-height: inherit;
            color: #797979;
            border: 0;
            border-bottom: 1px solid #e5e5e5;
        }
    </style>

    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-file-text-o"></i> Exam Creation</h3>
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="#">Home</a></li>
                <li><i class="icon_document_alt"></i>Exam></li>
                <li><i class="fa fa-file-text-o"></i>Create Exam</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <b><i class="fa fa-apple"></i> Exam Details</b>
                </div>
                    <div class="panel-body" style="padding-bottom: 4px;">
                        <form action="{{ route('postExamCreate') }}" method="POST" id="form_create_student" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <input type="hidden" name="class_id" id="class_id">
                            <input type="hidden" name="user_id" id="user_id" value="{{ Auth::id() }}">
                            <input type="hidden" name="dateregistered" id="dateregistered" value="{{ date('Y-m-d') }}">

                            <div class="row">
                                <div class="col-lg-9 col-md-9 col-sm-9">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exam_title">Exam Title*</label>
                                            <input type="text" name="exam_title" id="exam_title" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="number_question">number of question*</label>
                                            <input type="text" name="number_question" id="number_question" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="examDate">Exam Date*</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar dateOfBirth"></i>
                                                </div>
                                                <input type="text" name="examDate" id="examDate" class="form-control" placeholder="yyyy/mm/dd" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exam_time">Exam Duration*</label>
                                            <input type="time" name="exam_time" id="exam_time" step="1" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="marks_per_ques">marks per question*</label>
                                            <input type="text" name="marks_per_ques" id="marks_per_ques" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3">
                                
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="panel-footer">
                            <button value="submit" class="btn btn-default btn-save">Save <i class="fa fa-save"></i></button>
                        </div>
                     </form>
               </div>
          </div>
    </div>


@endsection

@section('script')
    <script>

        $('#form-multi-class #btn-go').addClass('hidden');

        $('#examDate').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd'
        });       
    </script>
@endsection