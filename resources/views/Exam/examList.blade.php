@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-file-text-o"></i>Exam List</h3>
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="#">Home</a></li>
                <li><i class="icon_document_alt"></i>Exam</li>
                <li><i class="fa fa-file-text-o"></i>Exam List</li>
            </ol>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table table-bordered table-hover table-striped table-condesed" id="Exam_search_table">
                <thead>
                    <th>N<sup>o</sup></th>
                    <th>Exam Title</th>
                    <th>Exam Date</th>
                    <th>Exam Duration</th>
                    <th>Total Marks</th>
                    <th>Total question</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach($exam as $key => $examData)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $examData['exam_title']}}</td>
                            <td>{{ date("d F Y",strtotime($examData['exam_date'])) }}</td>
                            <td>{{ $examData['duration']}}</td>
                            <td>{{ $examData['total_marks']}}</td>
                            <td>{{ $examData['no_question']}}</td>
                            <td>
                                 <a href="{{ route('questionInfo') }}?examId={{ $examData['exam_id'] }}"><button class="btn btn-default btn-save">View question </button></a>
                                 <a href="{{ route('assignExam') }}?examId={{ $examData['exam_id'] }}"><button class="btn btn-default btn-save">Assign Exam</button></a>
                                 <a href="{{ route('assignExamFetch')}}?examId={{ $examData['exam_id'] }}"><button class="btn btn-default btn-save">View Assign student</button></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="footer">
            {{ $exam->render() }}
        </div>
    </div>

@endsection

@section('script')
@endsection