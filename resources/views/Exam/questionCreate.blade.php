@extends('layouts.master')

@section('content')
    <style type="text/css">
        fieldset {
            margin-top: 5px;
        }

        fieldset legend {
            display: block;
            width: 100%;
            padding: 0;
            font-size: 15px;
            line-height: inherit;
            color: #797979;
            border: 0;
            border-bottom: 1px solid #e5e5e5;
        }
    </style>

    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-file-text-o"></i> Question Creation</h3>
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="#">Home</a></li>
                <li><i class="icon_document_alt"></i>Exam></li>
                <li><i class="fa fa-file-text-o"></i>Add Question</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <b><i class="fa fa-apple"></i> Question Details</b>
                </div>
                    <div class="panel-body" style="padding-bottom: 4px;">
                        <form action="{{ route('postquestionCreate') }}" method="POST" id="form_create_student" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <input type="hidden" name="class_id" id="class_id">
                            <input type="hidden" name="user_id" id="user_id" value="{{ Auth::id() }}">
                            <input type="hidden" name="noOfQuestion" id="noOfQuestion" value="{{ $questionRemain }}">
                            <input type="hidden" name="firstQuesId" id="firstQuesId" value="{{ $question_id }}">

                            <input type="hidden" name="examId" id="examId" value="{{ $examDetails['exam_id'] }}">
                            @for($i=1; $i<=$questionRemain; $i++)
                            <div class="row">
                                <div class="col-lg-9 col-md-9 col-sm-9">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="question_title">Question Title*</label>
                                            <input type="text" name="question_title_{{ $question_id + $i }}" id="question_title_{{ $question_id + $i }}" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="question_answer">Question Answers*</label>
                                            <input type="text" name="question_answer_{{ $question_id + $i }}" id="question_answer_{{ $question_id + $i }}" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="remain_opt_ques">Question Remaining Options*</label>
                                            <input type="text" name="remain_opt_ques_{{ $question_id + $i }}" id="remain_opt_ques_{{ $question_id + $i }}" class="form-control" placeholder="comma seprated option" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endfor
                        <br/>
                        <div class="panel-footer">
                            <button value="submit" class="btn btn-default btn-save">Save <i class="fa fa-save"></i></button>
                        </div>
                     </form>
               </div>
          </div>
    </div>


@endsection

@section('script')

@endsection