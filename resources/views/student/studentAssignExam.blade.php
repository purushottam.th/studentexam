@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-file-text-o"></i>Assign Exam Student List </h3>
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="#">Home</a></li>
                <li><i class="icon_document_alt"></i>Exam</li>
                <li><i class="fa fa-file-text-o"></i>Assign Exam student List</li>
            </ol>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table table-bordered table-hover table-striped table-condesed" id="Exam_search_table">
                <thead>
                    <th>N<sup>o</sup></th>
                    <th>Student Full Name</th>
                    <th>Exam Title</th>
                    <th>Exam Date</th>
                    <th>Exam Duration</th>
                    <th>Total Marks</th>
                    <th>Total question</th>
                    <th>Marks Obtain</th>
                    <th>Exam Appeard</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach($examAssignArray as $key => $examAssign)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $examAssign->first_name }} {{ $examAssign->last_name }}</td>
                            <td>{{ $examAssign->exam_title }}</td>
                            <td>{{ date("d F Y",strtotime($examAssign->exam_date)) }}</td>
                            <td>{{ $examAssign->duration }}</td>
                            <td>{{ $examAssign->total_marks }}</td>
                            <td>{{ $examAssign->no_question }}</td>
                            <td>{{ $examAssign->mark_obtain }}</td>
                            <td>{{ $examAssign->appeared }}</td>
                            <td>
                                @if( $examAssign->appeared == 'not appeared' && ($examAssign->exam_date == date("Y-m-d")))
                                <a href="{{ route('startExam')}}?exam_Id={{$examAssign->exam_id}}&&student_id={{$examAssign->student_id}}"><button class="btn btn-default btn-save">Start Exam</button></a>
                                @elseif( $examAssign->appeared == 'not appeared' && ($examAssign->exam_date >= date("Y-m-d")))
                                <button class="btn btn-default btn-save"> Exam To be Appered</button>
                                @else
                                <button class="btn btn-default btn-save"> Exam End</button>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
           </div>

@endsection

@section('script')
@endsection