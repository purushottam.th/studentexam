@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-file-text-o"></i>{{ $examDetails['exam_title'] }}</h3>
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="#">Home</a></li>
                <li><i class="icon_document_alt"></i>Exam</li>
                <li><i class="fa fa-file-text-o"></i>Question of {{ $examDetails['exam_title'] }}</li>
            </ol>
        </div>
    </div>
    <div class="panel panel-default">
        <form action="{{ route('postExamSubmit') }}" method="POST" id="form_create_student" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <input type="hidden" name="class_id" id="class_id">
            <input type="hidden" name="user_id" id="user_id" value="{{ Auth::id() }}">
            <input type="hidden" name="studentId" id="studentId" value="{{ $studentId }}">
            <input type="hidden" name="examId" id="examId" value="{{ $examId }}">

        <div class="panel-body">
            @if(count($questionDetails)==0)
            <span>no question added in this exam</span>
            @endif
            <?php $q=1; ?>
            @foreach($questionDetails as $questions)
            <div class="raw">
            <div class="col-lg-9 col-md-9 col-sm-9">
            <h3>Que {{ $q++ }}. {{ $questions['question_title'] }} ?</h3>
            <div class="row">
                <?php $options = $questions['question_option'].",".$questions['question_answer'];
                $optionArr= explode(",", $options);
                $first = shuffle($optionArr);                
                 ?>
                 @foreach($optionArr as $optionVal)
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <input type="radio" id="{{$optionVal}}_{{ $questions['question_id']}}" name="exam_{{$questions['question_id']}}" value="{{$optionVal}}">
                    <label for="{{$optionVal}}_{{ $questions['question_id']}}">{{$optionVal}}</label><br>
                </div>
                @endforeach
            </div>
            </div>
            </div>
            @endforeach
            
        </div>
        <br/>
        <div class="panel-footer">
            <button value="submit" class="btn btn-default btn-save">Save <i class="fa fa-save"></i></button>
        </div>
    </form>
        <div class="footer">
        </div>
    </div>

@endsection

@section('script')
@endsection